# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
  Backup for  MPLS testing on Cisco GNS3 Lab.
 - Control Plane using c7200's. OSPFv2, MPLS
 - PE-Routers using c7200 and running mBGP.
 - Edge routers using c7200's.
 - Sites connected to PE routers via OSPF, RIP, EIGRP & BGP for test purposes.

More details to be added.
![screenshot.png](https://bitbucket.org/repo/dknAKM/images/3901966951-screenshot.png)